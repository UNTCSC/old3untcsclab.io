---
layout: page
title: About Us
subtitle: Cybercops in training
---

### What is the UNT Cyber Security Club?

The University of North Texas Cyber Security Club provides students with an opportunity to learn about the concepts and techniques of [information security](https://en.wikipedia.org/wiki/Information_security) and to network with current security professionals in the north Texas region.

You can read our [constitution here](https://drive.google.com/file/d/0B9A3Z9_HMQERODhZUzRpR09PQmc/view?usp=sharing)

### I don't know anything about cyber security or even computers in general can I still join?

Yes! We don't expect you to be an engineering major or have any super in depth knowledge about computers. Our club is all about [teaching](https://youtu.be/KEkrWRHCDQU), mentoring, and providing people with the resources they need to learn. A number of our members have started off knowing nothing about cyber security and went on to become club officers and get jobs in the industry!

### When and where do you meet?

We normally meet on Fridays from 4:00 P.M. to 5:00 P.M. at Discovery Park in B185. Be sure to check our calendar as that will have the most up to date information in case there is an event going on that has us located somewhere else.

If you need to take the bus be sure to check out the [Discovery Park bus route](http://transportation.unt.edu/transit/maps-routes/discovery-park). The bus's normally stop running around 6:00 P.M. on Friday's. If you are in need of a ride to or from campus be sure to ask us and we can go about setting up a car pool.

We are also hoping to expand out and have some meetings on the main campus as we know Discovery Park is not the most convenient location for non engineering majors.

### I can't make that time is there anyway I can still be involved?

Sure! We have a [Slack](https://www.youtube.com/watch?v=9RJZMSsH7-g) server setup that acts as our main means of communication, if you want to join just use this [link](https://untccsi.slack.com/signup) and be sure to use your UNT email address. In addition we also have a number of subgroups that anyone is welcome to join who meet up every so often. You can also expect to see us posting what happens and what is going to happen at each meeting here on the site!

### What organizations are you involved with?

We are currently primarily involved with the North Texas chapter of [ISSA](https://ntxissa.org/). In addition to that we also attend local [OWASP](https://www.owasp.org/index.php/Main_Page) and [BSides](http://www.securitybsides.com/w/page/12194138/BSides) conferences and events.

If you are interested in getting involved with us send us an [email](mailto:untccsi@gmail.com)! We are always looking to expand our network of both on campus, off campus organizations, and with business's.

### What events do you take part in?

* [National Cyber League](https://www.nationalcyberleague.org/)
* [National Collegiate Cyber Defense Competition](http://www.nationalccdc.org/)
* Capture the Flag Competitions
* Hackathons
* Conferences
* Fun social events like LAN parties and board game nights
