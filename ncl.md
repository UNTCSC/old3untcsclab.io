---
layout: page
title: National Cyber League
---

### What is National Cyber League?

[National Cyber League](https://www.nationalcyberleague.org/) (NCL) is a program for providing participants a place to develop and practice their cyber security skills.

NCL will provide a series of challenges for both individuals (pre/regular season) and teams (postseason) to compete and then rank them against their peers.

<iframe width="560" height="315" src="https://www.youtube.com/embed/E0-pAP283zk" frameborder="0" allowfullscreen></iframe>

### Why should I participate?

1. You get good experience with various aspects of cyber security
2. You will learn something new almost guaranteed, we have had members site NCL as the reason they passed cert exams
3. It's a lot of fun
4. Being able to list "I competed in National Cyber League" on a resume
5. Facebook and other major tech companies sponsor the event and will be looking over results
6. At the end you get a cool scouting report for yourself that looks like [this](https://media.wix.com/ugd/766e9a_909c9c29f721456dba14fa92039730fa.pdf)(PDF)

### I don't know anything about cyber security though

That's okay a lot of our members start out in the same position! A number of challenges in NCL can be solved just by knowing how to use Google to look up the information you need. Be sure to check out our public resources here on the site as well, often times after an event we will have come across resources that really helped us and have put them up on it. The gymnasium you get access to through NCL also has a ton of tutorials on it that can help you out!

### How does registration work?

[From August 28th to September 29th registration is open](https://ncl.cyberskyline.com/events/ncl/fall/regular). Sign up fee is $25.

When it asks for Faculty/Coach please use Dr. Takabi's information:

* Hassan Takabi
* his_last_name[at]unt.edu

### Important Dates

| 8/28 - 9/29   | Registration is open                    |
| 9/30 - 10/1   | Late Registration for an additional $10 |
| 10/09         | Gym Opens, Credentials sent             |
| 10/20 - 10/28 | Mandatory Preseason Game                |
| 10/30         | Bracket assignments in Stadium          |
| 11/03 - 11/05 | Regular Season                          |
| 11/06 - 11/13 | Postseason Registration                 |
| 11/17 - 11/19 | Postseason Game                         |
| 12/16         | Fall Season Ends                        |